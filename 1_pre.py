import nltk
import pickle
import re
import os
from os import listdir
from os.path import isfile, join
from nltk.corpus import sentiwordnet

porter = nltk.PorterStemmer()
stop_words = set(['ourselves', 'hers', 'between', 'yourself', 'again', 'there', 'about', 'once', 'during', 'out', 'having', 'with', 'they', 'own', 'an', 'be', 'for', 'do', 'its', 'yours', 'such', 'into', 'of', 'is', 's', 'am', 'who', 'as', 'from', 'him', 'the', 'themselves', 'are', 'we', 'these', 'your', 'his', 'me', 'were', 'her', 'more', 'himself', 'this', 'our', 'their', 'to', 'ours', 'had', 'she', 'when', 'at', 'them', 'and', 'been', 'have', 'in', 'will', 'on', 'does', 'yourselves', 'then', 'that', 'because', 'what', 'why', 'now', 'he', 'you', 'herself', 'has', 'just', 'where', 'myself', 'which', 'those', 'i', 'after', 'few', 'whom', 't', 'being', 'theirs', 'my', 'a', 'by', 'doing', 'it', 'how', 'further', 'was', 'here', 'than'])

def is_word(word):
    return re.search('[a-z0-9]', word) != None

def read_file(path):
    with open(path, 'r', encoding="utf8") as myfile:
        return myfile.read()

def write_file(path, file_name, data):
    file = open(join(path, file_name), 'wb')
    pickle.dump(data, file)
    file.close()

def get_score(word):
    synsets = list(sentiwordnet.senti_synsets(word))
    if len(synsets) == 0:
        synsets = list(sentiwordnet.senti_synsets(porter.stem(word)))
    if len(synsets) == 0:
        return (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0)
    neg = [synset.neg_score() for synset in synsets]
    pos = [synset.pos_score() for synset in synsets]
    obj = [synset.obj_score() for synset in synsets]
    n = len(synsets)
    max_neg = max(neg)
    max_pos = max(pos)
    max_obj = max(obj)
    avr_neg = sum(neg)/n
    avr_pos = sum(pos)/n
    avr_obj = sum(obj)/n
    return (max_neg, max_pos, max_obj, avr_neg, avr_pos, avr_obj, n)

def run(path_in, path_out):
    if not os.path.exists(path_out):
        os.makedirs(path_out)
    print('Reading start')
    files = [(file_name, read_file(join(path_in, file_name))) for file_name in listdir(path_in) if isfile(join(path_in, file_name))]
    print('Reading finish')
    [write_file(path_out, file_name, [(porter.stem(word.lower()), pos) + get_score(word.lower()) for (word, pos) in nltk.pos_tag(nltk.wordpunct_tokenize(doc)) if word.lower() not in stop_words and is_word(word.lower())]) for (file_name, doc) in files]

run('.\\train\\neg\\', '.\\train\\pre_neg\\')
run('.\\train\\pos\\', '.\\train\\pre_pos\\')
run('.\\test\\neg\\', '.\\test\\pre_neg\\')
run('.\\test\\pos\\', '.\\test\\pre_pos\\')
