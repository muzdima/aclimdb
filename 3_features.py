import nltk
import itertools
import pickle
import os
from os import listdir
from os.path import isfile, join
import collections

def read_file(path, file_name):
    file = open(join(path, file_name), 'rb')
    result = pickle.load(file)
    file.close()
    return result

def write_file(path, file_name, data):
    file = open(join(path, file_name), 'wb')
    pickle.dump(data, file)
    file.close()

def split_grams(words, gap):
    return [(ngram[0], ngram[-1]) for ngram in list(nltk.ngrams(words, gap+2))]

path_dict = '.\\dict\\'
dict_1_grams = read_file(path_dict, '1-grams')
dict_2_grams = read_file(path_dict, '2-grams')
dict_2_1_grams = read_file(path_dict, '2-1-grams')
dict_2_2_grams = read_file(path_dict, '2-2-grams')
dict_pos_1_grams = read_file(path_dict, 'pos-1-grams')
dict_pos_2_grams = read_file(path_dict, 'pos-2-grams')

def write_features(path_out, file_name, data):
    words = [(vec[0], vec[1]) for vec in data]
    counter_1_grams = collections.Counter(list(nltk.ngrams(words, 1)))
    counter_2_grams = collections.Counter(list(nltk.ngrams(words, 2)))
    counter_2_1_grams = collections.Counter(list(split_grams(words, 1)))
    counter_2_2_grams = collections.Counter(list(split_grams(words, 2)))
    pos = [word[1] for word in words]
    counter_pos_1_grams = collections.Counter(list(nltk.ngrams(pos, 1)))
    counter_pos_2_grams = collections.Counter(list(nltk.ngrams(pos, 2)))

    values_1_grams = {'1_grams_'+str(grama):counter_1_grams[grama] if grama in counter_1_grams else 0 for grama in dict_1_grams}
    values_2_grams = {'2_grams_'+str(grama):counter_2_grams[grama] if grama in counter_2_grams else 0 for grama in dict_2_grams}
    values_2_1_grams = {'2_1_grams_'+str(grama):counter_2_1_grams[grama] if grama in counter_2_1_grams else 0 for grama in dict_2_1_grams}
    values_2_2_grams = {'2_2_grams_'+str(grama):counter_2_2_grams[grama] if grama in counter_2_2_grams else 0 for grama in dict_2_2_grams}
    values_pos_1_grams = {'pos_1_grams_'+str(grama):counter_pos_1_grams[grama] if grama in counter_pos_1_grams else 0 for grama in dict_pos_1_grams}
    values_pos_2_grams = {'pos_2_grams_'+str(grama):counter_pos_2_grams[grama] if grama in counter_pos_2_grams else 0 for grama in dict_pos_2_grams}

    nn = len(data)
    max_neg = [vec[2] for vec in data]
    max_pos = [vec[3] for vec in data]
    max_obj = [vec[4] for vec in data]
    max_neg_obj = [vec[2]*vec[4] for vec in data]
    max_pos_obj = [vec[3]*vec[4] for vec in data]
    avr_neg = [vec[5] for vec in data]
    avr_pos = [vec[6] for vec in data]
    avr_obj = [vec[7] for vec in data]
    avr_neg_obj = [vec[5]*vec[7] for vec in data]
    avr_pos_obj = [vec[6]*vec[7] for vec in data]
    n = [vec[8] for vec in data]

    max_max_neg = max(max_neg)
    max_max_pos = max(max_pos)
    max_max_obj = max(max_obj)
    max_max_neg_obj = max(max_neg_obj)
    max_max_pos_obj = max(max_pos_obj)
    max_avr_neg = max(avr_neg)
    max_avr_pos = max(avr_pos)
    max_avr_neg_obj = max(avr_neg_obj)
    max_avr_pos_obj = max(avr_pos_obj)
    max_avr_obj = max(avr_obj)
    max_n = max(n)
    avr_max_neg = sum(max_neg)/nn
    avr_max_pos = sum(max_pos)/nn
    avr_max_neg_obj = sum(max_neg_obj)/nn
    avr_max_pos_obj = sum(max_pos_obj)/nn
    avr_max_obj = sum(max_obj)/nn
    avr_avr_neg = sum(avr_neg)/nn
    avr_avr_pos = sum(avr_pos)/nn
    avr_avr_neg_obj = sum(avr_neg_obj)/nn
    avr_avr_pos_obj = sum(avr_pos_obj)/nn
    avr_avr_obj = sum(avr_obj)/nn
    avr_n = sum(n)//nn
    dif_max_max = max_max_pos - max_max_neg
    dif_max_max_obj = max_max_pos_obj - max_max_neg_obj
    dif_max_avr = max_avr_pos - max_avr_neg
    dif_max_avr_obj = max_avr_pos_obj - max_avr_neg_obj
    dif_avr_max = avr_max_pos - avr_max_neg
    dif_avr_max_obj = avr_max_pos_obj - avr_max_neg_obj
    dif_avr_avr = avr_avr_pos - avr_avr_neg
    dif_avr_avr_obj = avr_avr_pos_obj - avr_avr_neg_obj

    features = {}
    features = {**features, 'nn':nn, 'max_n':max_n, 'avr_n':avr_n}
    features = {**features, 'max_max_neg':max_max_neg, 'max_max_pos':max_max_pos, 'max_max_obj':max_max_obj, 'max_max_neg_obj':max_max_neg_obj, 'max_max_pos_obj':max_max_pos_obj}
    features = {**features, 'max_avr_neg':max_avr_neg, 'max_avr_pos':max_avr_pos, 'max_avr_neg_obj':max_avr_neg_obj, 'max_avr_pos_obj':max_avr_pos_obj, 'max_avr_obj':max_avr_obj}
    features = {**features, 'avr_max_neg':avr_max_neg, 'avr_max_pos':avr_max_pos, 'avr_max_neg_obj':avr_max_neg_obj, 'avr_max_pos_obj':avr_max_pos_obj, 'avr_max_obj':avr_max_obj}
    features = {**features, 'avr_avr_neg':avr_avr_neg, 'avr_avr_pos':avr_avr_pos, 'avr_avr_neg_obj':avr_avr_neg_obj, 'avr_avr_pos_obj':avr_avr_pos_obj, 'avr_avr_obj':avr_avr_obj}
    features = {**features, 'dif_max_max':dif_max_max, 'dif_max_max_obj':dif_max_max_obj, 'dif_max_avr':dif_max_avr, 'dif_max_avr_obj':dif_max_avr_obj}
    features = {**features, 'dif_avr_max':dif_avr_max, 'dif_avr_max_obj':dif_avr_max_obj, 'dif_avr_avr':dif_avr_avr, 'dif_avr_avr_obj':dif_avr_avr_obj}
    features = {**features, **values_1_grams, **values_2_grams, **values_2_1_grams, **values_2_2_grams, **values_pos_1_grams, **values_pos_2_grams}

    write_file(path_out, file_name, features)

def run(path_in, path_out):
    if not os.path.exists(path_out):
        os.makedirs(path_out)
    print('Reading start')
    data = [(file_name, read_file(path_in, file_name)) for file_name in listdir(path_in) if isfile(join(path_in, file_name))]
    print('Reading finish')
    [write_features(path_out, file_name, doc) for (file_name, doc) in data]

run('.\\train\\pre_neg\\', '.\\train\\features_neg\\')
run('.\\train\\pre_pos\\', '.\\train\\features_pos\\')
run('.\\test\\pre_neg\\', '.\\test\\features_neg\\')
run('.\\test\\pre_pos\\', '.\\test\\features_pos\\')
