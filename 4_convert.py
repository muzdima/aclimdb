import nltk
import itertools
import pickle
import os
from os import listdir
from os.path import isfile, join
import collections

def read_file(path, file_name):
    file = open(join(path, file_name), 'rb')
    result = pickle.load(file)
    file.close()
    return result

def format(s):
    s = s.replace('(', '_')
    s = s.replace('\'', '_')
    s = s.replace(')', '_')
    s = s.replace(',', '_')
    s = s.replace(' ', '_')
    return s

def write_features(path_out, features):
    with open(path_out,"w") as fp:
        fp.write('@RELATION imdb');
        fp.write('\n\n');
        fp.write('\n'.join(['@ATTRIBUTE '+format(k)+' NUMERIC' for (k,v) in features[0].items() if k!='label'] + ['@ATTRIBUTE label {neg,pos}']));
        fp.write('\n\n');
        fp.write('@DATA\n');
        fp.write('\n'.join([','.join([str(v) for (k,v) in feature.items() if k!='label'] + [feature['label']]) for feature in features]));
        fp.write('\n\n');

def run(path_in_neg, path_in_pos, path_out):
    print('Reading start')
    data_neg = [{**read_file(path_in_neg, file_name), **{'label': 'neg'}} for file_name in listdir(path_in_neg) if isfile(join(path_in_neg, file_name))]
    data_pos = [{**read_file(path_in_pos, file_name), **{'label': 'pos'}} for file_name in listdir(path_in_pos) if isfile(join(path_in_pos, file_name))]
    print('Reading finish')
    write_features(path_out, data_neg + data_pos)

run('.\\train\\features_neg\\', '.\\train\\features_pos\\', '.\\train\\data.arff')
run('.\\test\\features_neg\\', '.\\test\\features_pos\\', '.\\test\\data.arff')
