import nltk
import itertools
import pickle
import os
from os import listdir
from os.path import isfile, join
import collections

def read_file(path, file_name):
    file = open(join(path, file_name), 'rb')
    result = pickle.load(file)
    file.close()
    return result

def write_file(path, file_name, data):
    file = open(join(path, file_name), 'wb')
    pickle.dump(data, file)
    file.close()

def split_grams(words, gap):
    return [(ngram[0], ngram[-1]) for ngram in list(nltk.ngrams(words, gap+2))]

def run(paths):
    print('Reading start')
    data = [[read_file(path, file_name) for file_name in listdir(path) if isfile(join(path, file_name))] for path in paths]
    print('Reading finish')
    
    data = [[(vec[0], vec[1]) for vec in doc] for doc in list(itertools.chain.from_iterable(data))]
    counter1 = [k for (k,v) in collections.Counter(list(itertools.chain.from_iterable([list(nltk.ngrams(words, 1)) for words in data]))).items() if v >= 250]
    counter2 = [k for (k,v) in collections.Counter(list(itertools.chain.from_iterable([list(nltk.ngrams(words, 2)) for words in data]))).items() if v >= 250]
    counter3 = [k for (k,v) in collections.Counter(list(itertools.chain.from_iterable([list(split_grams(words, 1)) for words in data]))).items() if v >= 250]
    counter4 = [k for (k,v) in collections.Counter(list(itertools.chain.from_iterable([list(split_grams(words, 2)) for words in data]))).items() if v >= 250]
    pos = [[word[1] for word in words] for words in data]
    counter_pos1 = [k for (k,v) in collections.Counter(list(itertools.chain.from_iterable([list(nltk.ngrams(words, 1)) for words in pos]))).items() if v >= 250]
    counter_pos2 = [k for (k,v) in collections.Counter(list(itertools.chain.from_iterable([list(nltk.ngrams(words, 2)) for words in pos]))).items() if v >= 250]

    path_dict = '.\\dict\\'
    if not os.path.exists(path_dict):
        os.makedirs(path_dict)
    write_file(path_dict, '1-grams', counter1)
    write_file(path_dict, '2-grams', counter2)
    write_file(path_dict, '2-1-grams', counter3)
    write_file(path_dict, '2-2-grams', counter4)
    write_file(path_dict, 'pos-1-grams', counter_pos1)
    write_file(path_dict, 'pos-2-grams', counter_pos2)

run(['.\\train\\pre_neg\\', '.\\train\\pre_pos\\'])
